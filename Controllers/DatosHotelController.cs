﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hotel.Models;

namespace Hotel.Controllers
{
    public class DatosHotelController : Controller
    {
        private HotelContext db = new HotelContext();

        // GET: DatosHotel
        public async Task<ActionResult> Index()
        {
            return View(await db.DatosHoteles.ToListAsync());
        }

        // GET: DatosHotel/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatosHotel datosHotel = await db.DatosHoteles.FindAsync(id);
            if (datosHotel == null)
            {
                return HttpNotFound();
            }
            return View(datosHotel);
        }

        // GET: DatosHotel/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DatosHotel/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "DatosHotelId,Nombre,Direccion,Correo,Telefono")] DatosHotel datosHotel)
        {
            if (ModelState.IsValid)
            {
                db.DatosHoteles.Add(datosHotel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(datosHotel);
        }

        // GET: DatosHotel/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatosHotel datosHotel = await db.DatosHoteles.FindAsync(id);
            if (datosHotel == null)
            {
                return HttpNotFound();
            }
            return View(datosHotel);
        }

        // POST: DatosHotel/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "DatosHotelId,Nombre,Direccion,Correo,Telefono")] DatosHotel datosHotel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(datosHotel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(datosHotel);
        }

        // GET: DatosHotel/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DatosHotel datosHotel = await db.DatosHoteles.FindAsync(id);
            if (datosHotel == null)
            {
                return HttpNotFound();
            }
            return View(datosHotel);
        }

        // POST: DatosHotel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DatosHotel datosHotel = await db.DatosHoteles.FindAsync(id);
            db.DatosHoteles.Remove(datosHotel);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
