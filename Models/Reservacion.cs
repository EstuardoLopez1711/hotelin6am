﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class Reservacion
    {
        public int ReservacionId { get; set; }
        [Required]
        public DateTime FechaEntrada { get; set; }
        [Required]
        public DateTime FechaSalida { get; set; }

        public int HabitacionId { get; set; }
        public Habitacion Habitacion { get; set; }

        public int UserId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}