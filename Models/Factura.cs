﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class Factura
    {
        public int FacturaId { get; set; }
        [Required]
        public DateTime Fecha { get; set; }

        public int UserId { get; set; }
        public UserProfile UserProfile { get; set; }
    }
}