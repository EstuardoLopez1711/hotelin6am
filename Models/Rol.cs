﻿using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class Rol
    {
        public int RolId { get; set; }
        [Required]
        public string Nombre { get; set; }
    }
}