﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class DetalleFactura
    {
        public int DetalleFacturaId { get; set; }

        public int FacturaId { get; set; }
        public Factura Factura { get; set; }
        public int ReservacionId { get; set; }
        public Reservacion Reservacion { get; set; }
    }
}