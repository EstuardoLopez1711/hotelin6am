﻿using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class Estado
    {
        public int EstadoId { get; set; }
        [Required]
        public string Nombre { get; set; }
    }
}