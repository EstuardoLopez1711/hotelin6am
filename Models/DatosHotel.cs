﻿using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class DatosHotel
    {
        public int DatosHotelId { get; set; }

        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Direccion { get; set; }
        [Required]
        public string Correo { get; set; }
        [Required]
        public string Telefono { get; set; }
      
    }
}