﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class Administracion
    {
        public int AdministracionId { get; set; }

        public int UserId { get; set; }
        public UserProfile UserProfile { get; set; }

        public int DatosHotelId { get; set; }
        public DatosHotel DatosHotel { get; set; }
    }
}