﻿namespace Hotel.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class HotelContext : DbContext
    {

        public HotelContext()
            : base("name=HotelContext")
        {
        }
        public virtual DbSet<Rol> Roles { get; set; }
        public virtual DbSet<DatosHotel> DatosHoteles { get; set; }
        public virtual DbSet<Administracion> Administraciones { get; set; }
        public virtual DbSet<DetalleFactura> DetalleFacturas { get; set; }
        public virtual DbSet<Estado> Estados { get; set; }
        public virtual DbSet<Evento> Eventos { get; set; }
        public virtual DbSet<Factura> Facturas { get; set; }
        public virtual DbSet<Habitacion> Habitaciones { get; set; }
        public virtual DbSet<Reservacion> Reservaciones { get; set; }
        public virtual DbSet<Servicio> Servicios { get; set; }
        public virtual DbSet<ServicioHabitacion> ServicioHabitaciones { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }

    }

}
