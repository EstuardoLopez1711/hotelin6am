﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class Usuario
    {

        public int UsuarioId { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellido { get; set; }
        [Required]
        public string Direccion { get; set; }
        [Required]
        public string Correo { get; set; }
        [Required]
        public string Nick { get; set; }
        [Required]
        public string Contrasena { get; set; }
        [Required]
        public string Nit { get; set; }
        [Required]
        public string Imagen { get; set; }

        public int RolId { get; set; }
        public Rol Rol { get; set; }

    }
}