﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class Servicio
    {
        public int ServicioId { get; set; }
        [Required]
        public string Nombre { get; set; }
        public double Precio { get; set; }
        public int DatosHotelId { get; set; }
        public DatosHotel DatosHotel { get; set; }
    }
}