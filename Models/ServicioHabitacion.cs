﻿using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class ServicioHabitacion
    {
        public int ServicioHabitacionId { get; set; }
        public int ServicioId { get; set; }
        public Servicio Servicio { get; set; }
        public int ReservacionId { get; set; }
        public Reservacion Reservacion { get; set; }
        [Required]
        public string Descripcion { get; set; }
    }
}