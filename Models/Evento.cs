﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class Evento
    {
        public int EventoId { get; set; }
        [Required]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        [Required]
        public DateTime Fecha { get; set; }
        public string Imagen { get; set; }

        public int DatosHotelId { get; set; }
        public DatosHotel DatosHotel { get; set; }

       
    }
}