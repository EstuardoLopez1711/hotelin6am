﻿using System.ComponentModel.DataAnnotations;

namespace Hotel.Models
{
    public class Habitacion
    {
        public int HabitacionId { get; set; }
        [Required]
        public string Numero { get; set; }

        public int EstadoId { get; set; }
        public Estado Estado { get; set; }

        

    }
}